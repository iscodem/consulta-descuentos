package edu.senati.descuentossenati;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String TAG ="MainActivity";

    EditText edtNombres, edtApellidos, edtPromedio, edtSemestre;
    Button btnProcesar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG,">>>Ingresando al método onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombres = findViewById(R.id.edtNombres);
        edtApellidos = findViewById(R.id.edtApellidos);
        edtPromedio = findViewById(R.id.edtPromedio);
        edtSemestre = findViewById(R.id.edtSemestre);
        btnProcesar = findViewById(R.id.btnProcesar);
    }

    public void onProcesar(View view){
        Log.d(TAG,">>>Ingresando al método onProcesar()");

        String _nombres = edtNombres.getText().toString();
        String _apellidos = edtApellidos.getText().toString();
        String _semestre = edtSemestre.getText().toString();

        String mensaje = "Hola, "+_nombres+" "+_apellidos;
        Toast.makeText(this, mensaje, Toast.LENGTH_LONG).show();
    }


}
